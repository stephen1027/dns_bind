#!/bin/bash
docker stop bind
docker rm bind
docker run --name bind -d  --env ROOT_PASSWORD=xchl8192 -p 50001:22 --restart=always --publish 53:53/tcp --publish 53:53/udp --publish 10000:10000/tcp --volume /mnt/bind:/data ol_bind:latest 
