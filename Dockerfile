FROM sameersbn/bind:latest

RUN apt-get update
RUN apt-get install -y ssh
COPY build/entrypoint.sh /sbin/entrypoint.sh
COPY build/sshd_config /etc/ssh/sshd_config 
