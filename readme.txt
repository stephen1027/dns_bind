(1) rebuild bind image to support ssh
```
./build.sh
```

(2) run a container
```
 ./run_bind.sh
```

now you can do:
* login webmin https://<ip>:10000 with username/password as root/xchl8192
* login with ssh as : ssh root@<ip> -p 50001 , password is xchl8192

The modification of image:
* copy entrypoint.sh from the base image(sameersbn/bind:latest) and insert a command for starting sshd.
* copy sshd_config from current image(ol_bind:latest) and modify to support root login.
